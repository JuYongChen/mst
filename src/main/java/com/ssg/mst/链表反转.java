package com.ssg.mst;




//  Definition for singly-linked list.
class ListNode {
  int val;
  ListNode next;
  ListNode() {}
  ListNode(int val) { this.val = val; }
  ListNode(int val, ListNode next) { this.val = val; this.next = next; }
}


public class 链表反转 {

    public static void main(String[] args) {

        String a = "22";
        String b = "22";
        //输入：head = [1,2,3,4,5]
        //输出：[5,4,3,2,1]

        ListNode node = new ListNode(1);
        node.next = new ListNode(2);;
        node.next.next =new ListNode(3);
        node.next.next.next = new ListNode(4);
        node.next.next.next.next= new ListNode(5);
        //1->2->3->null

        ListNode prev = null, curr = node;

        while(curr != null){
            ListNode next = curr.next;
            curr.next = prev;  //null <- 1 <- 2 <-3
            prev = curr;  //指针迁移  prev =1
            curr = next;
        }

        //null <- 1 <- 2 <-3
        while (prev != null) {
            System.out.println(prev.val);
            prev = prev.next;

        }
    }
}
