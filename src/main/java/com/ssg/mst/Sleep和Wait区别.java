package com.ssg.mst;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Sleep和Wait区别 {

    public static void main(String[] args) {
        Object o = new Object();
        Thread thread = new Thread(() -> {
            synchronized (o) {
                System.out.println("新线程获取锁时间：" + LocalDateTime.now() + " 新线程名称：" + Thread.currentThread().getName());
                try {

                    //wait 释放cpu同时释放锁
                    o.wait(2000);

                    //sleep 释放cpu不释放锁
                    //Thread.sleep(2000);
                    System.out.println("新线程获取释放锁锁时间：" + LocalDateTime.now() + " 新线程名称：" + Thread.currentThread().getName());
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });

        thread.start();

        HashMap<Object, Object> map = new HashMap<>();
        Map<Object, Object> map1 = Collections.synchronizedMap(map);


        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        System.out.println("主线程获取锁时间：" + LocalDateTime.now() + " 主线程名称：" + Thread.currentThread().getName());

        synchronized (o){
            System.out.println("主线程获取释放锁锁时间：" + LocalDateTime.now() + " 主线程名称：" + Thread.currentThread().getName());
        }
    }
}
