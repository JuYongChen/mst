package com.ssg.mst;

public class 字符串反转 {

    public static void main(String[] args) {
        String a = "abcdefg";
        char[] s = new char[a.length()];
        for (int i = 0; i < a.length(); i++) {
            s[i] = a.charAt(i);
        }

        System.out.println(s);

        int left = 0;
        int right = a.length() - 1;
        while (left < right) {

            char temp = s[left];
            s[left] = s[right];
            s[right] = temp;

            left++;
            right--;
        }


        System.out.println(s);
    }
}
