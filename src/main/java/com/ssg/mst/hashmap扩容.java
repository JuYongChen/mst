package com.ssg.mst;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class hashmap扩容 {

    public static void main(String[] args) throws Exception {

        Map<Object, Object> map = new HashMap<>(2);
        //获取HashMap整个类
        Class<?> mapType = map.getClass();
        //获取指定属性，也可以调用getDeclaredFields()方法获取属性数组
        Field threshold = mapType.getDeclaredField("threshold");
        //将目标属性设置为可以访问
        threshold.setAccessible(true);

        Method capacity = mapType.getDeclaredMethod("capacity");
        //设置目标方法为可访问
        capacity.setAccessible(true);

        System.out.println("容量："+capacity.invoke(map)+"    阈值："+threshold.get(map)+"    元素数量："+map.size());

        for (int i = 0; i < 10; i++) {
            map.put(i,i);
        }

        //打印刚初始化的HashMap的容量、阈值和元素数量
        System.out.println("容量："+capacity.invoke(map)+"    阈值："+threshold.get(map)+"    元素数量："+map.size());


        for (int i = 11; i < 13; i++) {
            map.put(i,i);
        }

        System.out.println("容量："+capacity.invoke(map)+"    阈值："+threshold.get(map)+"    元素数量："+map.size());

        map.put(99,99);
        System.out.println("容量："+capacity.invoke(map)+"    阈值："+threshold.get(map)+"    元素数量："+map.size());

    }
}
