package com.ssg.mst;

public class StackOverFlowTest {
    private static int count = 1;
    public static void main(String[] args) {
        //模拟栈溢出
        //getDieCircle();

        //模拟堆溢出
        getOutOfMem();
    }

    public static void getDieCircle(){
        System.out.println(count++);
        getDieCircle();
    }

    public static void getOutOfMem(){
        while (true) {
            Object o = new Object();
            System.out.println(o);
        }
    }
}
